package downloader;
import java.net.*;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.io.*;

import javax.swing.SwingWorker;

// This is a decorator (wrapper) for an InputStream that you can monitor read progress.
//import javax.swing.ProgressMonitorInputStream;

/** 
 *  Download a URL to a File.
 *
 * @author James Brucker
 */
public class URLReader extends SwingWorker<Integer, Integer> implements Runnable {
	private static DownloaderUI ui;
	private URL url;
	private URLConnection connection;
	private InputStream instream;
	private int bytesRead; // number of bytes read so far
	private static final int BUFFERSIZE = 32*1024; // default buffer size for read
	/** length of URL stream, lazily determined. */
	private int urlsize;
	/** The output file. */
	private File outfile;
	/** output file to write to */
	private FileOutputStream outstream;
	
	/** 
	 * Initialize a new URL Reader.
	 * @param url is the URL to read from
	 * @param outfile is a File output to write output to. If it is a writable directory
	 *        then a file is created in that directory with same name as the download resource.
	 * @throws IOException if URLConnection fails, 
	 * @throws FileNotFoundException if outfile not found or cannot be written to
	 * @precondition url is a valid URL, outfile is a valid writable file or directory. 
	 */
	public URLReader( URL url, File outfile , DownloaderUI ui) throws IOException {
		if (url == null) throw new IllegalArgumentException("url cannot be null");
		this.url = url;
		this.ui = ui;
		this.bytesRead = 0;
		if (outfile == null) throw new IllegalArgumentException("output file cannot be null");
		if (! outfile.canWrite()) 
			throw new IllegalArgumentException("output file "+outfile.getName()+" is not writable");
		if ( outfile.isDirectory() ) {
			// create a file in directory having same name as the URL resource name
			String filename = url.getPath();
			int k = filename.lastIndexOf("/");
			if (k >= 0) {
				if (k == filename.length()-1) filename = "noname";
				else filename = filename.substring(k+1); // could fail
			}
			outfile = new File(outfile, filename); 
		}
		this.outfile = outfile;
		// don't open connection yet -- the server might close it before we run()
		// create output writer
		outstream = new FileOutputStream( outfile ); // "rwd" mode?
	}
	
	private InputStream getInputStream() throws IOException {
		connection = url.openConnection();
		return connection.getInputStream();
	}
	
	/**
	 * Get the number of bytes downloaded so far.
	 * @return number of bytes downloaded from URL so far.
	 */
	public int getBytesRead( ) { return bytesRead; }
	
	/** 
	 * Read the URL connection and save bytes to output file. 
	 * This method will block until the entire URL content is read.
	 * While reading, the bytesRead object attribute is regularly updated.
	 * @return the number of bytes actually read
	 */
	public int readURL( ) {
		bytesRead = 0;
		int buffsize = BUFFERSIZE;
		byte [] buff = new byte[buffsize];
		try {
			instream = getInputStream();
			int index=0;
			do {
				int n = instream.read( buff );
				if ( n < 0 ) break; // read returns -1 at end of input
				outstream.write(buff, 0, n);
				bytesRead += n;
				if(index<bytesRead/1024) {
					index = bytesRead/1024;
					publish(bytesRead);
				}
			} while ( true );
		} catch (IOException e) { 
			System.err.println("readURL: "+e); 
		} finally {
			if ( instream != null ) try { 
				instream.close(); 
			} catch (IOException e) { /* who cares? its not my data. */ }
			try {
				outstream.close();
			} catch (IOException e) { /* ignore it */ }
		}
		return bytesRead;
	}
	
	/** 
	 * Get the size in bytes of the URL to download.
	 * @return the size in bytes of the URL to download, -1 if cannot determine.
	 */
	public int getSize( ) {
		if ( urlsize > 0 ) return urlsize;
		if ( url == null ) return 0;
		try {
			URLConnection connection = url.openConnection();
			urlsize = connection.getContentLength();
		} catch (java.io.IOException e) {
			urlsize = -1;
		}
		return urlsize;
	}
	
	/**
	 * Get actual name of the output file.
	 * @return string name with path of the output file
	 */
	public String getOutputFile() {
		return outfile.getPath();
	}
	
	/** start the URL reader. On completion the output stream is closed. */
//	public void run() {
//		readURL( );
//	}
	
	/** test the code 
	 * @throws MalformedURLException 
	 */
	public static void main(String[] args) throws MalformedURLException {
		String URLSTRING = "http://www.ku.ac.th/web2012/resources/template2/ui/images/logo.png";
		URL url = new URL(URLSTRING);
		// this works on Linux that has a writable "/tmp" directory
		
		try {
			URLReader reader = new URLReader(url, new File("/tmp") ,ui);
			int bytes = reader.readURL();
			System.out.printf("Read %d bytes\n", bytes);
		} catch (IOException e) {
			System.out.println( e );
		}	
	}
	
	@Override
	protected void done(){
		try {
			ui.addMessage("\nDownloaded "+ get()+" bytes to " + getOutputFile());
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected Integer doInBackground() throws Exception {
		return readURL();
	}

	@Override
	protected void process(List<Integer> chunks) {
//		ui.addMessage("downloading..." + chunks.get(chunks.size()-1)+"\n");
		ui.getProgressBar().setValue(chunks.get(chunks.size()-1));
	}

	
}
